module Control.Monad.State.Composition where

import Control.Monad.State.Lazy

f :: (s -> t) -> (t -> s -> s) -> State t () -> State s ()
f getter setter trans = do
  x <- get
  let t1 = getter x
  let t2 = execState trans t1
  put $ setter t2 x

g :: (s -> t) -> (t -> s -> s) -> State t a -> (a -> State s b) -> State s b
g getter setter trans mutate = do
  x <- get
  let t1 = getter x
  let (a1, t2) = runState trans t1
  put $ setter t2 x
  mutate a1






